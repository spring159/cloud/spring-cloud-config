### Git Repository as Configuration Storage

This is a git repository for configuring cloud storage for microservices with Spring Cloud. Basically, **Spring Cloud Config** is a microservice where other microservices query the properties of the application itself. Once the “client” application is live, use the config server configuration and ask the Spring Cloud Config service what settings it needs to apply, identifying itself by application name.

The server uses physical files to store the per-application files. Of course, it wouldn't be very smart to use physical files because one of the assumptions is that the microservice must be able to die and be born without relying on the physical machine. And a file is a dependency. So it uses a *git* repository and checks it when it needs to refer to files.




### How to use git repository in a Spring Cloud Config Server Application

You are gonna need a Springboot Application with Spring Cloud dependency. There you will place the properties `spring.cloud.config.server.git.uri` in your application.yml or bootstrap.yml

### Example of usage
Check the repository link: [spring-cloud-config-server-repository](https://gitlab.com/spring159/cloud/spring-cloud-config-server/-/tree/master).
